all:
	game-menu dungeon-sample tic-sample lobby-sample mind-reader word-sample zoo-sample hang-sample cards-sample

%: %.cc
	g++ -std=c++11 $< -o $@

%: %.c
	gcc $< -o $@
	
clean:
	rm *.exe *.o
	
game-menu: game-menu.cc
	g++ -std=c++11 $< -o $@

test:
	make all && ./game-menu
