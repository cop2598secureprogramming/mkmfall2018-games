     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 

○	Feature Branch Name -- unknown-dungeon
○	Feature Branch Name -- tic tac toe
○	Feature Branch Name -- lobby
○	Feature Branch Name -- reader
○	Feature Branch Name -- word-herd
○	Feature Branch Name -- zoo-keeper
○	Feature Branch Name -- hang-man
○	Feature Branch Name -- black-jack
○	Developer Name -- Mary Mickelson
○	Developer Email -- mary.mickelson@smail.rasmussen.edu
○	Date Created -- 10/21/2018, 10/28/18, 11/04/18-2
○	Date Merged -- 10/21/2018, 10/28/18, 11/04/18-2
○	Approved By -- Mary Mickelson
○	Description -- added lobby, added Mind Reader, added Word Herd, added Critter Caretaker, added Hangman, added Blackjack


Hi there! Welcome to Cloud9 IDE!

To get you started with C/C++, we have created some small hello world
applications and a Makefile.

Have a look at the files, and use the terminal to build them:

    $ make
    $ ./hello-c-world
    $ ./hello-cpp-world

We're sure you can take it from there. Go ahead and edit the code, 
or add some new files. It's all up to you! 

Happy coding!
The Cloud9 IDE team


## Support & Documentation

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE. 
To watch some training videos, visit http://www.youtube.com/user/c9ide
