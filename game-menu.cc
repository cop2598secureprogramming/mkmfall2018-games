#include <iostream>
#include <limits>
#include "functions.h"

using namespace std;

// Tell us something about yourself
// Name /.- 
// General Location - 

// Set up your global variables here


// Define your functions here


int main()
{
    
    int choice;

    do
    {
        cout << "\nGAME MENU\n";
        cout << "0 - Exit the program.\n";
        cout << "1 - Dungeon Game\n";
        cout << "2 - Tic Tac Toe Game\n";
        cout << "3 - Game Lobby\n";
        cout << "4 - Mind Reader\n";
        cout << "5 - Word Herd\n";
        cout << "6 - Critter Caretaker\n";
        cout << "7 - Hangman\n";
        cout << "8 - Blackjack";
        cout << endl << "Enter choice: ";
        cin >> choice;

        switch (choice)
        {
            case 0: cout << "Good-bye Player.\n"; break;
            case 1: system("./dungeon-sample"); break;
            case 2: system("./tic-sample");break;
            case 3: system("./lobby-sample");break;
            case 4: system("./mind-reader");break;
            case 5: system("./word-sample");break;
            case 6: system("./zoo-sample");break;
            case 7: system("./hang-sample");break;
            case 8: system("./cards-sample");break;
            default: cout << "That was not a valid choice.\n";
        }
    }
    while (choice != 0);

    return 0;
}
